#ifndef __SPICY_RB_BST_REMOVE
#define __SPICY_RB_BST_REMOVE

void SPICY_rb_bst_remove(struct SPICY_rb_bst*, void*, uint64_t);

#endif
