#include <inttypes.h> // PRIu*
#include <stdint.h> // int*_t and uint*_t
#include <string.h> // memcmp()

#include "SPICY/core/data-types/rb-bst.h" // struct SPICY_rb_bst / struct SPICY_rb_bst_node

#include "SPICY/core/logging/log.h" // SPICY_log_*()

void* SPICY_rb_bst_find(struct SPICY_rb_bst *bst, void *index, uint64_t index_size){
	SPICY_log_debug("Finding node with index %p of size %"PRIu64"...", index, index_size);

	struct SPICY_rb_bst_node *current = bst->root_node;

	if(current != NULL && current->index_size != index_size)
		current = NULL;

	while(current != NULL){
		int direction = *(uint8_t*)current->index - *(uint8_t*)index;
		if(direction == 0){
			direction = memcmp(current->index, index, index_size);
			if(direction == 0){
				SPICY_log_debug1("Found node %p with index %p of size %"PRIu64"...", current, index, index_size);
				break;
			}
		}

		// And go deeper
		current = current->children[(direction < 0)];
	}

	// Now we finally return the result
	if(current == NULL){
		SPICY_log_debug("No node found with index %p of size %"PRIu64"...", index, index_size);
		return NULL;
	}

	return current->data;
}
