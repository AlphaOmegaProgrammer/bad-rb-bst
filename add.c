#include <inttypes.h> // PRIu*
#include <stddef.h> // NULL
#include <stdint.h> // int*_t and uint*_t
#include <string.h> // memcmp()

#include "SPICY/core/data-types/SPICY.h" // struct SPICY
#include "SPICY/core/data-types/rb-bst.h" // struct SPICY_rb_bst / struct SPICY_rb_bst_node

#include "SPICY/core/logging/log.h" // SPICY_log_*()

#include "SPICY/core/mem-pool/allocate.h" // SPICY_mem_pool_allocate()
#include "SPICY/core/mem-pool/free.h" // SPICY_mem_pool_free()

extern struct SPICY SPICY;

unsigned char SPICY_rb_bst_add(struct SPICY_rb_bst *bst, void* data, void* index, uint64_t index_size){
	unsigned char result = 1;

	// Set up the new node
	struct SPICY_rb_bst_node *node = SPICY_mem_pool_allocate(sizeof(struct SPICY_rb_bst_node));
	SPICY_log_debug_max("rb_bst_add: Allocating memory address %p", node);
	node->red = 1;

	node->index = index;
	node->index_size = index_size;

	node->data = data;

	node->parent = NULL;
	node->children[0] = NULL;
	node->children[1] = NULL;

	SPICY_log_debug1("Adding node %p (data %p, index %p of size %"PRIu64")...", node, data, index, index_size);

	if(bst->root_node == NULL){
		SPICY_log_debug1("Tree empty, adding node %p (data %p, index %p of size %"PRIu64")...", node, data, index, index_size);
		bst->root_node = node;
		node->red = 0;
		goto SPICY_RB_BST_ADD_END;
	}

	if(index_size != bst->root_node->index_size){
		result = 0;
		SPICY_mem_pool_free(sizeof(struct SPICY_rb_bst_node), node);
		goto SPICY_RB_BST_ADD_END;
	}

	node->parent = bst->root_node;

	for(;;){
		// Figure out if we're going left or right
		int direction = ((unsigned char*)node->parent->index)[0] - ((unsigned char*)node->index)[0];
		if(direction == 0){
			direction = memcmp(node->parent->index, node->index, node->index_size);
			if(direction == 0){
				// If the index already exists, don't add new node to prevent duplicate entries
				result = 0;
				SPICY_mem_pool_free(sizeof(struct SPICY_rb_bst_node), node);
				goto SPICY_RB_BST_ADD_END;
			}
		}

		direction = (direction < 0);

		// And either insert the node, or go deeper
		if(node->parent->children[direction] == NULL){
			node->parent->children[direction] = node;
			SPICY_log_debug_max("Added node %p as child[%i] of parent node %p", node, direction, node->parent);
			break;
		}

		node->parent = node->parent->children[direction];
	}


	SPICY_RB_BST_ADD_REBALANCING:
	SPICY_log_debug_max("Rebalancing tree...");

	unsigned char parent_direction = 0;
	struct SPICY_rb_bst_node *parent = node->parent, *grandparent = NULL, *uncle = NULL;

	if(parent != NULL)
		grandparent = parent->parent;

	if(grandparent != NULL){
		parent_direction = (grandparent->children[1] == parent);
		uncle = grandparent->children[!parent_direction];
	}

	SPICY_log_debug_max("Current %p Parent %p Grandparent %p Uncle %p", node, parent, grandparent, uncle);

	if(parent == NULL || !parent->red){
		SPICY_log_debug_max("Parent was NULL or black... Rebalancing done");
		goto SPICY_RB_BST_ADD_END;
	}

	if(grandparent == NULL){
		SPICY_log_debug_max("Grandparent was NULL... Painting parent black. Rebalancing done");
		parent->red = 0;
		goto SPICY_RB_BST_ADD_END;
	}

	if(uncle == NULL || !uncle->red){
		SPICY_log_debug_max("Uncle null or black...");

		if(parent->children[!parent_direction] == node){
			SPICY_log_debug_max("Node is inner grandchild... Rotating node with parent");

			grandparent->children[parent_direction] = node;

			parent->parent = node;
			parent->children[!parent_direction] = node->children[parent_direction];

			node->parent = grandparent;
			node->children[parent_direction] = parent;

			if(parent->children[!parent_direction] != NULL)
				parent->children[!parent_direction]->parent = parent;

			parent = node;
			node = node->children[parent_direction];
		}

		SPICY_log_debug_max("Node is outer grandchild... Rotating parent with grandparent. Rebalancing done");

		if(grandparent->parent == NULL){
			SPICY_log_debug_max("Rotated parent is new root node, updating bst root node pointer...");
			bst->root_node = parent;
		}else
			grandparent->parent->children[grandparent->parent->children[1] == grandparent] = parent;

		parent->parent = grandparent->parent;
		grandparent->parent = parent;

		grandparent->children[parent_direction] = parent->children[!parent_direction];
		parent->children[!parent_direction] = grandparent;

		if(grandparent->children[parent_direction] != NULL)
			grandparent->children[parent_direction]->parent = grandparent;


		parent->red = 0;
		grandparent->red = 1;
		node->red = 1;

		goto SPICY_RB_BST_ADD_END;
	}

	SPICY_log_debug_max("Parent and uncle red... painting grandparent red and parent and uncle black and continuing rebalancing...");

	grandparent->red = 1;
	parent->red = 0;
	uncle->red = 0;
	node = grandparent;

	goto SPICY_RB_BST_ADD_REBALANCING;



	SPICY_RB_BST_ADD_END:

	return result;
}
