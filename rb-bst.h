#ifndef __SPICY_RB_BST
#define __SPICY_RB_BST

#include <stdint.h> // int*_t and uint*_t

// 0 = left child, 1 = right child
enum SPICY_rb_bst_round_direction{
	SPICY_RB_BST_ROUND_DOWN = 0,
	SPICY_RB_BST_ROUND_UP = 1
};

struct SPICY_rb_bst_node{
	uint64_t red : 1;
	uint64_t index_size : 63;

	void *index, *data;

	struct SPICY_rb_bst_node *parent, *children[2]; // It makes the implementation easier to have a children array rather than *left/*right
};

struct SPICY_rb_bst{
	void *data;
	struct SPICY_rb_bst_node *root_node;
};

#endif
