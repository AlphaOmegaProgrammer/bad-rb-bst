#ifndef __SPICY_RB_BST_ADD
#define __SPICY_RB_BST_ADD

#include "SPICY/core/data-types/rb-bst.h" // struct SPICY_rb_bst / struct SPICY_rb_bst_node

unsigned char SPICY_rb_bst_add(struct SPICY_rb_bst*, void*, void*, uint64_t);

#endif
