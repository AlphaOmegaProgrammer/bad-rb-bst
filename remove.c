#include <inttypes.h> // PRIu*
#include <stdint.h> // int*_t and uint*_t
#include <string.h> // memcmp()

#include "SPICY/core/data-types/SPICY.h" // struct SPICY
#include "SPICY/core/data-types/rb-bst.h" // struct SPICY_rb_bst / struct SPICY_rb_bst_node

#include "SPICY/core/logging/log.h" // SPICY_log_*()

#include "SPICY/core/mem-pool/free.h" // SPICY_mem_pool_free()

extern struct SPICY SPICY;

void SPICY_rb_bst_remove(struct SPICY_rb_bst *bst, void *index, uint64_t index_size){
	SPICY_log_debug("Removing node with index %p of size %"PRIu64"...", index, index_size);

	struct SPICY_rb_bst_node *current = bst->root_node, *removed_node = NULL;

	if(current != NULL && current->index_size != index_size)
		goto SPICY_RB_BST_REMOVE_END;


	int current_direction;
	while(current != NULL){
		// Figure out if we're going left or right, or if we've found the node
		current_direction = ((unsigned char*)current->index)[0] - ((unsigned char*)index)[0];
		if(current_direction == 0){
			current_direction = memcmp(current->index, index, index_size);
			if(current_direction == 0)
				break;
		}

		// If this isn't the node, go deeper
		current = current->children[current_direction < 0];
	}

	if(current == NULL)
		return;

	removed_node = current;
	if(current->parent != NULL)
		current_direction = current->parent->children[1] == current;

	// If we couldn't find the node... We're done I guess
	if(!SPICY_compare_log_debug(current != NULL, "RB BST failed to find node with index %p size %"PRIu64, index, index_size))
		goto SPICY_RB_BST_REMOVE_END;

	if(bst->root_node == current){
		SPICY_log_debug1("Found node %p is the root node %p with index %p of size %"PRIu64". Removing...", current, bst->root_node, index, index_size);
	}else{
		SPICY_log_debug1("Found node %p with index %p of size %"PRIu64". Removing...", current, index, index_size);
	}


	SPICY_RB_BST_REMOVE_SIMPLE_CASES:
	SPICY_log_debug_max("Current node %p has %i children", current, (current->children[0] != NULL) + (current->children[1] != NULL));
	switch((current->children[0] != NULL) + (current->children[1] != NULL)){
		case 0:
			if(bst->root_node == current){
				SPICY_log_debug_max("Removed node %p with no children and was the root node. Simply removing...", current);
				bst->root_node = NULL;
				goto SPICY_RB_BST_REMOVE_END;
			}

			// Remove the current node from the tree
			current->parent->children[current_direction] = NULL;

			if(current->red == 1){
				SPICY_log_debug_max("Removed node %p with no children and was red. Simply removing...", current);
				goto SPICY_RB_BST_REMOVE_END;
			}

			// If the removed node was black... we continue to rebalancing
			goto SPICY_RB_BST_REMOVE_START_REBALANCE;
		break;

		case 1:
			SPICY_log_debug_max("Removed node %p has only one child. This node is black, and the child is guaranteed to be red. Replacing with child node and then coloring black...", current);

			current = current->children[current->children[0] == NULL];
			current->parent = removed_node->parent;
			current->red = 0;

			if(bst->root_node == removed_node)
				bst->root_node = current;
			else
				current->parent->children[current_direction] = current;

			goto SPICY_RB_BST_REMOVE_END;
		break;

		case 2:
			SPICY_log_debug_max("Removed node %p was root node with both children. Finding successor...", current);
			struct SPICY_rb_bst_node *successor = current->children[0];
			while(successor->children[1] != NULL)
				successor = successor->children[1];

			SPICY_log_debug_max("Successor found %p. Swapping successor and current nodes (except color)...", successor);

			void* tmp = current->index;
			current->index = successor->index;
			successor->index = tmp;

			tmp = current->data;
			current->data = successor->data;
			successor->data = tmp;

			uint64_t tmp_index_size = current->index_size;
			current->index_size = successor->index_size;
			successor->index_size = tmp_index_size;

			SPICY_log_debug_max("Successor %p and current %p swapped (except color).  Current node is now guaranteed to not have two children %p %p... Restarting simple cases check", successor, current, successor->children[0], successor->children[1]);

			current = successor;
			removed_node = current;
			current_direction = current->parent->children[1] == current;

			// This goes back to one of the 0 or 1 child cases
			goto SPICY_RB_BST_REMOVE_SIMPLE_CASES;
		break;
	}


	SPICY_RB_BST_REMOVE_REBALANCING:
	SPICY_log_debug_max("Rebalancing tree...");
	// We can assume the node is not the root node, is black, has no children, and has a sibling

	if(current->parent == NULL)
		goto SPICY_RB_BST_REMOVE_END;

	current_direction = current->parent->children[1] == current;

	SPICY_RB_BST_REMOVE_START_REBALANCE:;

	struct SPICY_rb_bst_node *parent = current->parent, *sibling = parent->children[!current_direction], *close_nephew = sibling->children[current_direction], *distant_nephew = sibling->children[!current_direction];

	SPICY_log_debug_max("Current %p Parent %p Sibling %p Close Nephew %p Distant Nephew %p", current, parent, sibling, close_nephew, distant_nephew);

	// First, if sibling is red, we have to rotate sibling and parent
	if(sibling->red){
		SPICY_log_debug_max("Sibling is red. Must rotate sibling and parent, color old parent black, and color new parent red...");
		
		// Get parent pointer so we can replace the parent with the sibling at the end
		struct SPICY_rb_bst_node **parent_pointer;
		if(parent->parent == NULL)
			parent_pointer = &(bst->root_node);
		else
			parent_pointer = &(parent->parent->children[parent->parent->children[1] == parent]);


		// Update parent pointer on close_nephew
		if(close_nephew != NULL)
			close_nephew->parent = parent;

		// Update pointers on sibling
		sibling->children[current_direction] = parent;
		sibling->parent = parent->parent;

		// Now update pointers on parent
		parent->children[!current_direction] = close_nephew;
		parent->parent = sibling;

		// Update *parent_pointer
		*parent_pointer = sibling;

		// Now update colors
		parent->red = 1;
		sibling->red = 0;

		// Now update local variables so we can continue
		sibling = close_nephew;
		close_nephew = sibling->children[current_direction];
		distant_nephew = sibling->children[!current_direction];
	}

	// Now, if close_nephew is red and distant_nephew is black, we have to rotate close_nephew and sibling
	if((close_nephew != NULL && close_nephew->red == 1) && (distant_nephew == NULL || distant_nephew->red == 0)){
		// close_nephew can't be NULL in this if statement
		SPICY_log_debug_max("Close nephew is red and distant nephew is black. Must rotate sibling and close nephew, color old sibling red, and color new sibling black...");

		// Update parent pointer on close_nephew distant child (if not NULL)
		if(close_nephew->children[!current_direction] != NULL)
			close_nephew->children[!current_direction]->parent = sibling;

		// Update pointers on sibling
		sibling->children[current_direction] = close_nephew->children[!current_direction];
		sibling->parent = close_nephew;

		// Update pointers on close_nephew
		close_nephew->parent = parent;
		close_nephew->children[!current_direction] = sibling;

		// Update parent children pointer
		parent->children[!current_direction] = close_nephew;

		// Now update colors
		sibling->red = 1;
		close_nephew->red = 0;

		// Now update local variables so we can continue
		distant_nephew = sibling;
		sibling = close_nephew;
		close_nephew = sibling->children[current_direction];
		// Note that distant nephew is forced to be red here
	}

	// Now if the distant newphew is red, we have to rotate sibling and parent
	if(distant_nephew != NULL && distant_nephew->red == 1){
		// distant_mephew can't be NULL in this if statement
		SPICY_log_debug_max("Distant nephew is red. Must rotate parent and sibling, color new parent the same color as the old parent, color old parent black, and color distant nephew black...");

		// Get parent pointer so we can replace the parent with the sibling at the end
		struct SPICY_rb_bst_node **parent_pointer;
		if(parent->parent == NULL)
			parent_pointer = &(bst->root_node);
		else
			parent_pointer = &(parent->parent->children[parent->parent->children[1] == parent]);

		// Update parent pointer on close_nephew distant child (if not NULL)
		if(close_nephew != NULL)
			close_nephew->parent = parent;

		// Update pointers on sibling
		sibling->children[current_direction] = parent;
		sibling->parent = parent->parent;

		// Now update pointers on parent
		parent->children[!current_direction] = close_nephew;
		parent->parent = sibling;

		// Update *parent_pointer
		*parent_pointer = sibling;

		// Now update colors
		sibling->red = parent->red;
		parent->red = 0;
		distant_nephew->red = 0;

		// And we're done
		goto SPICY_RB_BST_REMOVE_END;
	}

	// Now if the parent is red, make sibling red and parent black and we're done
	if(parent->red == 1){
		SPICY_log_debug_max("Parent is red. Must color sibling red and parent black...");
		sibling->red = 1;
		parent->red = 0;
		goto SPICY_RB_BST_REMOVE_END;
	
	}

	// Finally, all nodes are black, so we make the sibling black and continue rebalancing up the tree
	SPICY_log_debug_max("All relevant nodes are black. Must color sibling red,..");
	sibling->red = 1;
	if(parent != bst->root_node){
		SPICY_log_debug_max("Parent node is not root node. Must move up one level (make parent into current) and keep rebalancing");
		current = parent;
		goto SPICY_RB_BST_REMOVE_REBALANCING;
		// If parent is the root node, we're already all the way up the tree, so rebalancing is done
	}


	SPICY_RB_BST_REMOVE_END:

	if(removed_node != NULL)
		SPICY_mem_pool_free(sizeof(struct SPICY_rb_bst_node), removed_node);
}
