#ifndef __SPICY_RB_BST_FIND_NEAREST
#define __SPICY_RB_BST_FIND_NEAREST

#include <stdint.h> // int*_t and uint*_t

#include "SPICY/core/data-types/rb-bst.h" // struct SPICY_rb_bst / struct SPICY_rb_bst_node

void* SPICY_rb_bst_find_round(struct SPICY_rb_bst*, void*, uint64_t, enum SPICY_rb_bst_round_direction);

#endif
