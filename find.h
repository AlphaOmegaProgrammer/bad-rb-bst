#ifndef __SPICY_RB_BST_FIND
#define __SPICY_RB_BST_FIND

#include <stdint.h> // int*_t and uint*_t

#include "SPICY/core/data-types/rb-bst.h" // struct SPICY_rb_bst / struct SPICY_rb_bst_node

void* SPICY_rb_bst_find(struct SPICY_rb_bst*, void*, uint64_t);

#endif
