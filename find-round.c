#include <inttypes.h> // PRIu*
#include <stdint.h> // int*_t and uint*_t
#include <string.h> // memcmp()

#include "SPICY/core/data-types/rb-bst.h" // struct SPICY_rb_bst / struct SPICY_rb_bst_node

#include "SPICY/core/logging/log.h" // SPICY_log_*()

void* SPICY_rb_bst_find_round(struct SPICY_rb_bst *bst, void *index, uint64_t index_size, enum SPICY_rb_bst_round_direction round_direction){
	SPICY_log_debug("Finding nearest node with index %p of size %"PRIu64"...", index, index_size);

	struct SPICY_rb_bst_node *current = bst->root_node;

	if(current != NULL && current->index_size != index_size)
		current = NULL;

	uint64_t last_direction = round_direction;
	uint8_t branches_travelled[2] = {0};

	while(current != NULL){
		// Figure out if we're going left or right, or if we've found the node
		int direction = ((unsigned char*)current->index)[0] - ((unsigned char*)index)[0];
		if(direction == 0){
			direction = memcmp(current->index, index, index_size);
			if(direction == 0){
				SPICY_log_debug1("Found node %p with index %p of size %"PRIu64"...", current, index, index_size);
				break;
			}
		}

		direction = (direction < 0);
		last_direction = direction;
		branches_travelled[direction] = 1;

		// If the next node is NULL, we need to find the nearest instead
		if(current->children[direction] == NULL){
			if(last_direction == round_direction){
				if(!branches_travelled[!round_direction])
					current = NULL;
				else
					current = current->parent;
			}

			break;
		}

		current = current->children[direction];
	}

	// Now we finally return the result
	if(current == NULL){
		SPICY_log_debug("No node found with index %p of size %"PRIu64"...", index, index_size);
		return NULL;
	}

	return current->data;
}
